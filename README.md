# Swift Feed Code Generator

## Installation

Swift Feed Code Generator requires [Node.js](https://nodejs.org/) v10+ to run.

Install the dependencies.

```sh
npm install
```


## Create Swift Feed

Edit `inputForm.json` file

```json
{
  "projectName": "TestFeedCodeGen",
  "author": "John Doe",
  "controllerName": "FeedList",
  "controllerTemplate": "TableViewVC.swift",
  "cells": [
    "InputTypeData => YourCellName",
    "User => NewUserCell",
    "String => ArticleCell",
    "Blog => BlogCell"
  ]
}
```

Then run

```sh
ts-node index.ts
```

Output files will store in `./outputs` folder

## License

MIT

**Free Software, Hell Yeah!**
