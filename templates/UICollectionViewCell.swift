//
//  {{CLASS_NAME}}.swift
//  {{PROJ_NAME}}
//
//  Created by {{AUTH_NAME}} on {{DATE}}.
//

import UIKit

class {{CLASS_NAME}}: UICollectionViewCell, ConfigurableCell {
    
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization cod
        layer.borderColor = UIColor.red.cgColor
        layer.borderWidth = 1.0
        label.layer.borderColor = UIColor.black.cgColor
        label.layer.borderWidth = 1.0
    }

    func configure(data: String) {
        label.text = data
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        label.preferredMaxLayoutWidth = layoutAttributes.size.width - contentView.layoutMargins.left - contentView.layoutMargins.left
        layoutAttributes.bounds.size.height = systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        return layoutAttributes
    }
}
