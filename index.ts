import fs from 'fs';
import dayjs from 'dayjs';

type ReplaceConfig = {
  pattern: string;
  value: string;
};

type FileMold = {
  fileName: string;
  templateName: string;
  replaceTargets: ReplaceConfig[];
};

type FileContent = {
  fileName: string;
  contents: string;
  extension: string;
};

type CellConfigurator = {
  cellName: string;
  inputType: string;
  type: string;
};

const writeFileFromContent = async (file: FileContent): Promise<boolean> => {
  return new Promise((resolve, reject) => {
    fs.writeFile(
      `./outputs/${file.fileName}.${file.extension}`,
      file.contents,
      err => {
        if (err) {
          reject(err);
        }
        resolve(true);
      },
    );
  });
};

const getCellTemplateName = (controllerTemplate: string): string => {
  switch (controllerTemplate) {
    case 'CollectionViewVC.swift':
      return 'UICollectionViewCell';
    default:
      return 'UITableViewCell';
  }
};

const getTemplateString = (filePath: string): Promise<string> => {
  const path = `./templates/${filePath}`;
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf-8', (err, data) => {
      if (err) {
        reject(err);
      }
      resolve(data);
    });
  });
};

const getCellType = (templateName: string): string => {
  const pattern = /(UI|ViewCell|\.swift|\.xib)/g;
  return templateName.replace(pattern, '');
};

const createCellConfig = (
  text: string,
  templateName: string,
): CellConfigurator => {
  const cellPattern: RegExp = /(.+)\s=>\s(.+)/;
  const matches = text.match(cellPattern);
  if (matches) {
    return {
      cellName: matches[2],
      inputType: matches[1],
      type: getCellType(templateName),
    };
  }
  throw new Error('Cell is invalid');
};

const createCellConfigs = (
  cells: string[],
  templateName: string,
): CellConfigurator[] => {
  return cells.map(
    (cell): CellConfigurator => {
      return createCellConfig(cell, templateName);
    },
  );
};

const createTypeAlias = (cellConfig: CellConfigurator): string => {
  return `typealias ${cellConfig.cellName}Config = ${cellConfig.type}CellConfigurator<${cellConfig.cellName}, ${cellConfig.inputType}>`;
};

const createTypeAliases = (cellConfigs: CellConfigurator[]): string => {
  return cellConfigs.map(cell => createTypeAlias(cell)).join('\n');
};

const createCellConfiguratorSample = (cellConfig: CellConfigurator): string => {
  return `${cellConfig.cellName}Config(item: ${cellConfig.inputType}())`;
};

const createCellConfiguratorSamples = (
  cellConfigs: CellConfigurator[],
): string => {
  return cellConfigs
    .map(cell => createCellConfiguratorSample(cell))
    .join(',\n\t\t\t');
};

const createCellNames = (cellConfigs: CellConfigurator[]): string => {
  return cellConfigs.map(cell => `"${cell.cellName}"`).join(', ');
};

const createViewControllerMold = (
  template: string,
  headers: ReplaceConfig[],
  cellMolds: CellConfigurator[],
): FileMold => {
  let fileNameConfig = headers.find(header => header.pattern === 'CLASS_NAME');

  if (!fileNameConfig) {
    throw new Error('No required information');
  }

  let cellDefineConfig: ReplaceConfig = {
    pattern: 'CELL_NAMES',
    value: createCellNames(cellMolds),
  };

  let replaceTargets: ReplaceConfig[] = [...headers, cellDefineConfig];

  return {
    fileName: fileNameConfig.value,
    templateName: template,
    replaceTargets,
  };
};

const createCellConfiguratorMold = (
  template: string,
  headers: ReplaceConfig[],
): FileMold => {
  let targets = headers.filter((config): boolean => {
    return config.pattern !== 'CLASS_NAME';
  });

  targets.push({
    pattern: 'CLASS_NAME',
    value: 'CellConfigurator',
  });

  return {
    fileName: 'CellConfigurator',
    templateName: template,
    replaceTargets: targets,
  };
};

const createControllerPresenterMold = (
  template: string,
  headers: ReplaceConfig[],
  cellConfigs: CellConfigurator[],
): FileMold => {
  let fileNameConfig = headers.find(
    header => header.pattern === 'PRESENTER_NAME',
  );

  if (!fileNameConfig) {
    throw new Error('No required information');
  }
  let targets = headers.filter((config): boolean => {
    return config.pattern !== 'CLASS_NAME';
  });

  targets.push({
    pattern: 'CLASS_NAME',
    value: fileNameConfig.value,
  });

  let typeAliases: ReplaceConfig = {
    pattern: 'TYPE_ALIASES',
    value: createTypeAliases(cellConfigs),
  };

  let samples: ReplaceConfig = {
    pattern: 'CELL_CONFIGURATORS',
    value: createCellConfiguratorSamples(cellConfigs),
  };

  let replaceTargets: ReplaceConfig[] = [...targets, typeAliases, samples];

  return {
    fileName: fileNameConfig.value,
    templateName: template,
    replaceTargets,
  };
};

const createCellMold = (
  template: string,
  headers: ReplaceConfig[],
  cellConfig: CellConfigurator,
): FileMold => {
  let targets = headers.filter((config): boolean => {
    return config.pattern !== 'CLASS_NAME';
  });

  targets.push({
    pattern: 'CLASS_NAME',
    value: cellConfig.cellName,
  });

  let inputType: ReplaceConfig = {
    pattern: 'INPUT_TYPE',
    value: cellConfig.inputType,
  };

  targets.push(inputType);

  return {
    fileName: cellConfig.cellName,
    templateName: template,
    replaceTargets: targets,
  };
};

const createCellMolds = (
  template: string,
  headers: ReplaceConfig[],
  cellConfigs: CellConfigurator[],
): FileMold[] => {
  return cellConfigs.map(
    (config): FileMold => {
      return createCellMold(template, headers, config);
    },
  );
};

const createCellNib = (
  template: string,
  headers: ReplaceConfig[],
  cellConfig: CellConfigurator,
): FileMold => {
  let targets = headers.filter((config): boolean => {
    return config.pattern !== 'CLASS_NAME';
  });

  targets.push({
    pattern: 'CLASS_NAME',
    value: cellConfig.cellName,
  });

  return {
    fileName: cellConfig.cellName,
    templateName: template,
    replaceTargets: targets,
  };
};

const createCellNibsMolds = (
  template: string,
  headers: ReplaceConfig[],
  cellConfigs: CellConfigurator[],
): FileMold[] => {
  return cellConfigs.map(
    (config): FileMold => {
      return createCellNib(template, headers, config);
    },
  );
};

const createFileHeaders = (inputForm: any): ReplaceConfig[] => {
  return [
    {
      pattern: 'CLASS_NAME',
      value: inputForm.controllerName + 'ViewController',
    },
    {
      pattern: 'AUTH_NAME',
      value: inputForm.author,
    },
    {
      pattern: 'PROJ_NAME',
      value: inputForm.projectName,
    },
    {
      pattern: 'PRESENTER_NAME',
      value: inputForm.controllerName + 'Presenter',
    },
    {
      pattern: 'PRESENTER_PROTOCOL',
      value: inputForm.controllerName + 'Protocol',
    },
    {
      pattern: 'DATE',
      value: dayjs().format('DD/MM/YYYY'),
    },
  ];
};

const createMolds = (): FileMold[] => {
  const inputForm = require('./inputForm.json');

  const headers: ReplaceConfig[] = createFileHeaders(inputForm);
  const cellConfigs: CellConfigurator[] = createCellConfigs(
    inputForm.cells,
    getCellTemplateName(inputForm.controllerTemplate),
  );

  const viewControllerMold: FileMold = createViewControllerMold(
    inputForm.controllerTemplate,
    headers,
    cellConfigs,
  );

  const presenterMold: FileMold = createControllerPresenterMold(
    'Presenter.swift',
    headers,
    cellConfigs,
  );

  const cellConfiguratorMold: FileMold = createCellConfiguratorMold(
    'CellConfigurator.swift',
    headers,
  );

  let cellMolds: FileMold[] = createCellMolds(
    getCellTemplateName(inputForm.controllerTemplate) + '.swift',
    headers,
    cellConfigs,
  );

  let nibMolds: FileMold[] = createCellNibsMolds(
    getCellTemplateName(inputForm.controllerTemplate) + '.xib',
    headers,
    cellConfigs,
  );

  const molds = [
    viewControllerMold,
    presenterMold,
    cellConfiguratorMold,
    ...cellMolds,
    ...nibMolds,
  ];

  return molds;
};

const getTemplateExtension = (templateName: string): string => {
  const pattern = /(.+)\.(swift|xib)/;
  const matches = templateName.match(pattern);
  if (matches) {
    return matches[2];
  }
  throw new Error('Extension not found');
};

const createNewFileStringFromMold = (
  fileMold: FileMold,
): Promise<FileContent> => {
  return new Promise(async (resolve, reject) => {
    try {
      const template = await getTemplateString(fileMold.templateName);

      const fileString = fileMold.replaceTargets.reduce(
        (mainContent, target) => {
          const regex = new RegExp(`{{${target.pattern}}}`, 'g');
          return mainContent.replace(regex, target.value);
        },
        template,
      );

      resolve({
        fileName: fileMold.fileName,
        contents: fileString,
        extension: getTemplateExtension(fileMold.templateName),
      });
    } catch (error) {
      reject(error);
    }
  });
};

const main = async (): Promise<void> => {
  const molds: FileMold[] = createMolds();

  const tasks: Promise<FileContent>[] = molds.map(mold =>
    createNewFileStringFromMold(mold),
  );
  const fileContents: FileContent[] = await Promise.all(tasks);

  const writeTasks: Promise<boolean>[] = fileContents.map(file =>
    writeFileFromContent(file),
  );

  const writeStatus: boolean[] = await Promise.all(writeTasks);

  console.log({ writeStatus });
};

main();
