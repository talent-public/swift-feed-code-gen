//
//  {{CLASS_NAME}}.swift
//  {{PROJ_NAME}}
//
//  Created by {{AUTH_NAME}} on {{DATE}}.
//

import UIKit

class {{CLASS_NAME}}: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var presenter: {{PRESENTER_NAME}}!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        presenter = {{PRESENTER_NAME}}()
        presenter.fetchData()

        let cellNames = [{{CELL_NAMES}}]
        registerTableViewCells(cellNames: cellNames, tableView: self.tableView)
        
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        tableView.tableFooterView = UIView()
    }

    func registerTableViewCells(cellNames: [String], tableView: UITableView) {
        let nibs = cellNames.map { (cellName) -> (UINib, String) in
            let nib = UINib(nibName: cellName, bundle: nil)
            return (nib, cellName)
        }
        
        nibs.forEach { (nib, cellName) in
            tableView.register(nib, forCellReuseIdentifier: cellName)
        }
    }
}

extension {{CLASS_NAME}}: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfItems
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = presenter.item(at: indexPath)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: item).reuseId)!
        item.configure(cell: cell)

        return cell
    }
}

