//
//  {{CLASS_NAME}}.swift
//  {{PROJ_NAME}}
//
//  Created by {{AUTH_NAME}} on {{DATE}}.
//

import Foundation

protocol {{PRESENTER_PROTOCOL}} {
    
}

{{TYPE_ALIASES}}

class {{CLASS_NAME}} {
    
    var items: [CellConfigurator] = []
    
    var numberOfItems: Int {
        return items.count
    }
    
    init() {
        
    }
    
    func fetchData() {
        items = [
            {{CELL_CONFIGURATORS}}
        ]
    }
    
    func item(at indexPath: IndexPath) -> CellConfigurator {
        return items[indexPath.item]
    }
}
