//
//  {{CLASS_NAME}}.swift
//  {{PROJ_NAME}}
//
//  Created by {{AUTH_NAME}} on {{DATE}}.
//

import UIKit

class {{CLASS_NAME}}: UITableViewCell, ConfigurableCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    public func configure(data: {{INPUT_TYPE}}) {
        // ... setup {{CLASS_NAME}} ... //
        
    }
    
}
