//
//  FeedListPresenter.swift
//  TestFeedCodeGen
//
//  Created by Peerasak Unsakon on 11/03/2021.
//

import Foundation

protocol FeedListProtocol {
    
}

typealias NewUserCellConfig = TableCellConfigurator<NewUserCell, String>
typealias ArticleCellConfig = TableCellConfigurator<ArticleCell, String>
typealias BlogCellConfig = TableCellConfigurator<BlogCell, String>

class FeedListPresenter {
    
    var items: [CellConfigurator] = []
    
    var numberOfItems: Int {
        return items.count
    }
    
    init() {
        
    }
    
    func fetchData() {
        items = [
            NewUserCellConfig(item: String()),
			ArticleCellConfig(item: String()),
			BlogCellConfig(item: String())
        ]
    }
    
    func item(at indexPath: IndexPath) -> CellConfigurator {
        return items[indexPath.item]
    }
}
