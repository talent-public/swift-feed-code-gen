//
//  {{CLASS_NAME}}.swift
//  {{PROJ_NAME}}
//
//  Created by {{AUTH_NAME}} on {{DATE}}.
//

import UIKit

class {{CLASS_NAME}}: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var presenter: {{PRESENTER_NAME}}!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        presenter = {{PRESENTER_NAME}}()
        
        let cellNames = [{{CELL_NAMES}}]
        registerCollectionViewCells(cellNames: cellNames, collectionView: self.tableView)

        collectionView.backgroundColor = .white
        collectionView.contentInsetAdjustmentBehavior = .always
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        guard let collectionViewLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        
        collectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        collectionViewLayout.sectionInsetReference = .fromLayoutMargins
        
        presenter.fetchData()
    }

    func registerCollectionViewCells(cellNames: [String], collectionView: UICollectionView) {
        let nibs = cellNames.map { (cellName) -> (UINib, String) in
            let nib = UINib(nibName: cellName, bundle: nil)
            return (nib, cellName)
        }
        
        nibs.forEach { (nib, cellName) in
            collectionView.register(nib, forCellWithReuseIdentifier: cellName)
        }
    }
}

extension {{CLASS_NAME}}: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = presenter.item(at: indexPath)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: type(of: item).reuseId, for: indexPath)
        item.configure(cell: cell)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sectionInset = (collectionViewLayout as! UICollectionViewFlowLayout).sectionInset
        let referenceHeight: CGFloat = 100 // Approximate height of your cell
        let referenceWidth = collectionView.safeAreaLayoutGuide.layoutFrame.width
            - sectionInset.left
            - sectionInset.right
            - collectionView.contentInset.left
            - collectionView.contentInset.right
        return CGSize(width: referenceWidth, height: referenceHeight)
    }
}

